import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.functions.SMO;
import weka.classifiers.trees.RandomForest;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import weka.core.Attribute;
import weka.core.Instance;
import java.util.ArrayList;
import java.lang.String;

public class RunMining {
    public enum classifierType { SVM, RF};
    public static Instances trainingInstances, testingInstances;
    public static double [][] theMatrix = null;
 
    /**
    * Create objects of type Instances read from arff files
    *
    * @param  trainFileName     name of arff file containing 50% of records, to be used for training
    * @param  testFileName      name of arff file containing remaining 50%, used for testing
    */
    public static void createInstances(String trainFileName, String testFileName){
        trainingInstances = null; testingInstances = null;
        try{
            DataSource source = new DataSource(trainFileName);
            trainingInstances = source.getDataSet();
            if (trainingInstances.classIndex() == -1)
                trainingInstances.setClassIndex(trainingInstances.numAttributes() - 1);

            source = new DataSource(testFileName);
            testingInstances = source.getDataSet();
            if (testingInstances.classIndex() == -1)
                testingInstances.setClassIndex(testingInstances.numAttributes() - 1);

        } catch (Exception ex) {
            Logger.getLogger(RunMining.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
    * Uses training data to create model and saves it in a .model file
    *
    * @param  cType             type of classifier
    */
    public static void trainClassifier(classifierType cType){
        try {
            Classifier classifier;
            String classifierFilename;
            switch(cType){
                case SVM:   classifier = (Classifier) new SMO();
                            classifierFilename = "SMO.model";
                    break;
                case RF:    classifier = (Classifier) new RandomForest();
                            classifierFilename = "RF.model";
                    break;
                default:    System.out.println("Invalid classifier type"); return;
            }
            
            //build the classifier for the training instances
            classifier.buildClassifier(trainingInstances);

            // Serializing the classsifier object -> storing the trained classifier in to a file for later use
            weka.core.SerializationHelper.write(classifierFilename, classifier);
        } catch (Exception ex) {
            Logger.getLogger(RunMining.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
    * Reads created model from file and performs evaluation, using training instances
    *
    * @param  cType             type of classifier
    * @param  resultsFilename   name of file containing basic statistics provided by weka evaluation
    */
    public static void evaluateClassifier(classifierType cType, String resultsFilename){
        try {
            Classifier classifier;
            Evaluation eval;
            StringBuilder tmp;
            String classifierFilename, result;
            FileWriter fw;
            BufferedWriter bw;
            File file;
            boolean isNewFile = false;

            switch(cType){
                case SVM:   classifierFilename = "SMO.model"; break;
                case RF:    classifierFilename = "RF.model"; break;
                default:    System.out.print("Invalid classifier type"); return;
            }
            //load the trained classifer from the file - desrialization
            classifier =(Classifier) weka.core.SerializationHelper.read(classifierFilename);

            eval = new Evaluation(trainingInstances);
            eval.evaluateModel(classifier, testingInstances);

            file = new File(resultsFilename);                
            if(!file.exists()){
                file.createNewFile();
                isNewFile = true;
            }

            tmp = new StringBuilder(); // Using default 16 character size
            if(isNewFile){
                tmp.append("pctCorrect,kappaStatistics,meanAbsoluteError,rootMeanSquaredError\n");
                isNewFile = false;
            }
            tmp.append(eval.pctCorrect() );             tmp.append(",");
            tmp.append(eval.kappa());                   tmp.append(",");
            tmp.append(eval.meanAbsoluteError());       tmp.append(",");
            tmp.append(eval.rootMeanSquaredError());    tmp.append("\n");
            result = tmp.toString();

            //writing into file            
            fw = new FileWriter(resultsFilename, true);
            bw = new BufferedWriter(fw);
            bw.write(result);
            bw.close();
            
            if(theMatrix == null)
                theMatrix = new double [eval.confusionMatrix().length][eval.confusionMatrix().length];

            for(int i=0; i<theMatrix.length; i++){
                for(int j=0; j<theMatrix.length; j++){
                    theMatrix[i][j] += eval.confusionMatrix()[i][j];
                }
            }            

            System.out.println("Evaluation result: " + eval.correct() + "\t" + eval.pctCorrect() + "%\n");
            /*  //writing out some statistics
            System.out.println(eval.toMatrixString());
            System.out.println(eval.toClassDetailsString());
            System.out.println("incorrect: " + eval.incorrect() + "\t" + eval.pctIncorrect() + "%");
            System.out.println("Kappa statistics: " + eval.kappa());
            System.out.println("correlation coefficient: " + eval.correlationCoefficient() ); //java.lang.Exception: Can't compute correlation coefficient: class is nominal!
            System.out.println("mean absolute error: " + eval.meanAbsoluteError());
            System.out.println("root mean squared error: " + eval.rootMeanSquaredError());
            System.out.println("unclassified: " + eval.unclassified() + "\t" + eval.pctUnclassified() + "%");
            System.out.println(eval.toSummaryString("\nResults " + cType + "\n======\n", false));
            //*/            
        } catch (Exception ex) {
            Logger.getLogger(RunMining.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
    * Uses training data to create model and saves it in a .model file
    *
    * @param  trainFileName     name of arff file containing 50% of records, to be used for training
    * @param  testFileName      name of arff file containing remaining 50%, used for testing
    * @param  cType             type of classifier
    * @param  randomize         true, if the tissues should be assigned randomly to records
    * @param  resultsFilename   name of file containing basic statistics provided by weka evaluation
    */
    public static void runTest(String trainFileName, String testFileName, classifierType cType, boolean randomize, String resultsFilename){
        /*** Creates training and testing sets from given files ***/
        createInstances(trainFileName, testFileName);  

        if(randomize) {
            int seed = (int)System.currentTimeMillis();
            randomizeClassVector(trainingInstances, seed);
            randomizeClassVector(testingInstances, seed);
        }  

        /*** Training classifier - creating and saving models ***/
        trainClassifier(cType);
        System.out.println("Training completed\n");  
        evaluateClassifier(cType, resultsFilename);      
    }

    /**
    * Constracts a unique name for each type of test performed
    *
    * @param  cType             type of classifier
    * @param  important         true, if test performed on file limited to important genes only
    * @param  randomize         true, if the tissues should be assigned randomly to records
    * @return outputFileName    base for output files with results of test performed
    */
    public static String outputFileName(classifierType cType, boolean important, boolean randomize){

        StringBuilder tmp = new StringBuilder(); // Using default 16 character size

        switch(cType){
            case SVM:   tmp.append("resultSVM/SVM"); break;
            case RF:    tmp.append("resultRF/RF"); break;
            default:    System.out.print("Invalid classifier type"); return null;
        }

        if(important)
            tmp.append("_imp");
        else
            tmp.append("_full");

        if(randomize)
            tmp.append("_rand");

        return tmp.toString();
    }

    /**
    * Iterates through tests, saving statistics, average confusion matrix and percentage result matrix
    *
    * @param  trainFileName     name of arff file containing 50% of records, to be used for training
    * @param  testFileName      name of arff file containing remaining 50%, used for testing
    * @param  cType             type of classifier
    * @param  times             number of times the test should be run
    * @param  important         true, if test performed on file limited to important genes only
    * @param  randomize         true, if the tissues should be assigned randomly to records
    */
    public static void repeatTests(String trainFileName, String testFileName,  classifierType cType, int times, boolean important, boolean randomize){
        String outputFile, matrixFile, result, baseName;
        String percentFile, percentResult, heatName;
        StringBuilder tmp = new StringBuilder(); // Using default 16 character size
        StringBuilder tmp2 = new StringBuilder(); // Using default 16 character size
        File file, file2;
        Double[][] matrixPercent;
        Double sum;
        String suffix;

        baseName = outputFileName(cType, important, randomize);
        outputFile = baseName + ".csv";
        matrixFile = baseName + "_mtrx.csv";
        percentFile = baseName + "_mtrx_pct.m";
        if (baseName.indexOf('/')!=-1)
            heatName = baseName.substring(baseName.indexOf('/')+1); 
        else
            heatName = baseName;

        //Remove files if exist
        try{
            file = new File(matrixFile);
            if(file.exists())
                file.delete();
            file = new File(outputFile);
            if(file.exists())
                file.delete();
            file = new File(percentFile);
            if(file.exists())
                file.delete();
        } catch (Exception ex) {
            Logger.getLogger(RunMining.class.getName()).log(Level.SEVERE, null, ex);
        }
      
        //run test
        if(important)
            suffix = "-im.arff";
        else
            suffix = ".arff";
        for(int i=0; i<times; i++){
            System.out.println("Test\t\t" + (i+1) + "\n");            
            runTest(trainFileName + (i+1) + suffix, testFileName + (i+1) + suffix, cType, randomize, outputFile);            
        }

        //average matrix
        for(int i=0; i<theMatrix.length; i++){
            for(int j=0; j<theMatrix.length; j++){
                theMatrix[i][j] /= times;
                tmp.append(theMatrix[i][j]);             
                tmp.append(",");
            }
            tmp.append("\n");
        }
        result = tmp.toString();

        try{
            file = new File(matrixFile);
            if(!file.exists())
                file.createNewFile();
            FileWriter fw = new FileWriter(matrixFile, true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(result);
            bw.close();
        } catch (Exception ex) {
            Logger.getLogger(RunMining.class.getName()).log(Level.SEVERE, null, ex);
        }        
        
        /*	
        
			Below will do the following:
			for each row in theMatrix[][]:
				sum up the row, cell by cell, and save it in sum
				compute the percentages of each cell in the row and put in the cell in matrixPercent[][]
			
			Start making the heatmap strings with tmp2 
			
			make new / reset file ( "RFPercentMatrix.m"/"SVMPercentMatrix.m" )
			
			Write the heatmap strings to ( "RFPercentMatrix.m"/"SVMPercentMatrix.m" )
				
		*/

        System.out.println("--Getting sum of the matrix and writing percentages to new double[][] row-wise--");
        matrixPercent = new Double[theMatrix.length][theMatrix.length];
        tmp2.append(heatName + " = " + "["); 

        for(int i=0; i<theMatrix.length; i++){
			sum = 0.0;
            for(int j=0; j<theMatrix.length; j++){
                sum = sum + theMatrix[i][j];
                }                
			for(int j=0; j<theMatrix.length; j++){
                matrixPercent[i][j] = theMatrix[i][j]/sum*100.0;

                tmp2.append(Math.floor(matrixPercent[i][j] * 100) / 100);           //make heatmap strings from percentages             
                tmp2.append(" ");
			}
            if (i < matrixPercent.length-1 ) {
                tmp2.append(";");
            }
        }	
        System.out.println("-------------------making heatmap strings from percentages----------------------");
		tmp2.append("];"); 
		tmp2.append("\nimagesc(" + heatName + ");\ncolormap(flipud(gray));");
		percentResult = tmp2.toString();
        System.out.println("-----------------------writing heatmap strings to file--------------------------");

        try{
            file2 = new File(percentFile);
            if(!file2.exists())
                file2.createNewFile();
            FileWriter fw2 = new FileWriter(percentFile, true);
            BufferedWriter bw2 = new BufferedWriter(fw2);
            bw2.write(percentResult);
            bw2.close();
        } catch (Exception ex) {
            Logger.getLogger(RunMining.class.getName()).log(Level.SEVERE, null, ex);
        }
		System.out.println("--------------------------------closing file------------------------------------");		
    }

    /**
    * Reads tissues of records and reassigns them randomly
    *
    * @param  instances     set of instances to perform random assignment on
    * @param  seed          input for random number generator
    */
    public static void randomizeClassVector(Instances instances, int seed) {
        ArrayList<String> tissueList = new ArrayList<String>();

        for(int i=0; i<instances.numInstances(); i++) {
            Instance currentInstance = instances.instance(i);
            int tissuePos = currentInstance.numAttributes()-1;
            double valuepos = currentInstance.value(currentInstance.attribute(tissuePos));
            String tempstr = currentInstance.attribute(tissuePos).value((int) valuepos);
            tissueList.add(tempstr);
        }
        // Randomize tissueList
        Random random = new Random(seed);
        Collections.shuffle(tissueList, random);

        for(int i=0; i<tissueList.size(); i++) {
            instances.instance(i).setValue((instances.numAttributes()-1),tissueList.get(i));
        }
        return;
    }
    
    public static void main(String[] args) throws Exception {
            //repetTests(base name of training set, base name of testing set, classifier type, how many runs, if important, if randomize)
            //*
            repeatTests("training", "testing", RunMining.classifierType.RF, 5, false, false);
            repeatTests("training", "testing", RunMining.classifierType.RF, 5, false, true);
            repeatTests("training", "testing", RunMining.classifierType.RF, 20, true, false);
            repeatTests("training", "testing", RunMining.classifierType.RF, 20, true, true);

            repeatTests("training", "testing", RunMining.classifierType.SVM, 20, false, false);
            repeatTests("training", "testing", RunMining.classifierType.SVM, 20, false, true);
            repeatTests("training", "testing", RunMining.classifierType.SVM, 20, true, false);
            repeatTests("training", "testing", RunMining.classifierType.SVM, 20, true, true);
            //*/
    }
}