#!/usr/bin/python
import sys
import pickle
import os.path

if not os.path.isfile("./genes.dict"):
	line_c = 0
	of = open("Homo_sapiens.GRCh37.75.gtf",'r')
	gene_transl = dict()
	for line in of:
		if line_c >=5:
			v = line.split('\t')
			r = v[-1].split(';')
			for i in range(len(r)):
				if "gene_id" in r[i]:
					l = r[i].split('"')
					gene_id = l[1]
				if "gene_name" in r[i]:
					s = r[i].split('"')
					gene_name = s[1]
			gene_transl[gene_id]=gene_name
		line_c += 1
	#print len(gene_transl)
	of = open("genes_info.txt")
	for line in of:
		v = line.split('\t')
		gene_name = v[1]
		gene_id = v[8]
		if not gene_transl.has_key(gene_name):
			gene_transl[gene_id]=gene_name
	with open('genes.dict', 'wb') as handle:
		pickle.dump(gene_transl, handle)

if not os.path.isfile("./important_genes.dict"):
	important_genes = set()
	of = open("cancer_gene_census.tsv",'r')
	skip = 0
	for line in of:
		if skip == 1:
			v = line.split('\t')
			gene_symbol = v[0]
			if gene_symbol not in important_genes:
				important_genes.add(gene_symbol)
		else:
			skip = 1
	with open('important_genes.dict', 'wb') as handle:
		pickle.dump(important_genes, handle)

with open('genes.dict', 'rb') as handle:
	gene_transl = pickle.loads(handle.read())
with open('important_genes.dict', 'rb') as handle:
	important_genes = pickle.loads(handle.read())

currentFile = open(sys.argv[1],'r')
output_set = set()
output_set_important = set()
for line in currentFile:
	v = line.split(',')
	tissue = sys.argv[2].split('-')
	try:
		gene = gene_transl[v[1].strip('\n')]
	except Exception, e:
		gene = v[1].strip('\n')
	result = v[0] + ',' + gene + ',' + tissue[0] + '\n'
	if result not in output_set:
		output_set.add(result)
	if gene in important_genes:
		output_set_important.add(result)
with open(sys.argv[2], "a") as outputfile:
	for elem in output_set:
		outputfile.write(elem)
with open(sys.argv[3], "a") as outputfile:
	for elem in output_set_important:
		outputfile.write(elem)
