#!/usr/bin/python
import sys
import os.path
from random import shuffle
from collections import defaultdict

mergedfile = './'+sys.argv[1]+'.csv'
outputname = './'+sys.argv[1]+'.arff'
if (sys.argv[1] == "mergedim"):
	trainingFile = './datafiles-arff/training'+sys.argv[2]+'-im.arff'
	testingFile = './datafiles-arff/testing'+sys.argv[2]+'-im.arff'
else:
	trainingFile = './datafiles-arff/training'+sys.argv[2]+'.arff'
	testingFile = './datafiles-arff/testing'+sys.argv[2]+'.arff'
if os.path.isfile(mergedfile):
	of = open(mergedfile,'r')
	i = 0 # used for patient ids
	j = 0 # used for gene ids
	k = 0 # tissue ids (0..25)
	gene_id = dict() # key: gene name, value: gene int id (j)
	patient_id = dict() # key: patient name, value: patient int id (i)
	#patient_name = dict() #reversed dict of patient_id. Since patient_id is a 1:1 dict, there might be a better way
	patient_tissue = dict() # key: patient int id (i), value: tissue name
	tissue_id = dict() # key: tissue name, value: tissue id (0-25)
	for line in of: # create int id for each patient and gene
		v = line.split(',')
		patient = v[0]
		gene = v[1]
		tissue = v[2]
		if not patient_id.has_key(patient):
			patient_id[patient] = i
			#patient_name[i] = patient
			patient_tissue[i] = tissue.replace("\n", "")
			if not tissue_id.has_key(tissue.replace("\n", "")):
				tissue_id[tissue.replace("\n", "")] = k
				k = k+1
			i = i+1
		if not gene_id.has_key(gene):
			gene_id[gene] = j
			j = j+1
	matrix = defaultdict(list) # matrix[x].append(y) then matrix[x].sort() for all x
	of = open(mergedfile,'r')
	for line in of:
		v = line.split(',')
		curr_patient_id = patient_id[v[0]]
		curr_gene_id = gene_id[v[1]]
		if curr_gene_id not in matrix[curr_patient_id]: # MIGHT FIX THE TODO BELOW
			matrix[curr_patient_id].append(curr_gene_id) # TODO: curr_patient_id might map to a patient already present in the matrix..








	# Create list of sets
	# each set contains all patients with the same tissue type
#	tissueArr = [[] for index in xrange(26)]
#	tissueArr = [[0 for x in xrange(5)] for x in xrange(26)]

	tissueArr = [ [] for x in range(26) ]

	for x in range(i-1):
		curPatientTissue = patient_tissue[x]
		curTissue = tissue_id[curPatientTissue]
		tissueArr[curTissue].append(x)
	


	#print tissue_id['BOCA']
	#print tissueArr
	

	for x in tissueArr:
		shuffle(x)


	trainingPatients = []
	testPatients = []

	for x in tissueArr:
		y = 0
		#print 'Tissue: '+patient_tissue[x[0]]+'\t'+str(len(x))
		numTrainingPatients = len(x)//2
		while (y<numTrainingPatients):
			trainingPatients.append(x[y])
			y = y+1
		while (y<len(x)):
			testPatients.append(x[y])
			y = y+1
	#print tissueArr[0]
	#print "Training patients:"
	#print len(trainingPatients)
	#print "Test patients:"
	#print len(testPatients)
	#print "Training + test patients:"
	#print len(trainingPatients)+len(testPatients)
	#print "Total patients in matrix:"
	#print len(matrix)


	training_data_set = list()
	test_data_set = list()

	for patient in matrix:	
		matrix[patient].sort()
		result = '{'#0 '+patient_name[patient]+', '
		for curr_gene_id in matrix[patient]:
			result = result + str(curr_gene_id) + ' 1, ' #fjernet +1 ved curr_gene_id
		result = result + str(j) + ' ' + patient_tissue[patient] + '}' #fjernet +1 ved j
		if (trainingPatients.count(patient) > 0):
			training_data_set.append(result)

		if (testPatients.count(patient) > 0):
			test_data_set.append(result)
	


	if os.path.isfile(trainingFile):
		os.remove(trainingFile)

	if os.path.isfile(testingFile):
		os.remove(testingFile)


	## CREATE TRAINING FILE
	with open(trainingFile, "a") as outputfile:
		outputfile.write("% DM838 - Project 2+4\n")
   		outputfile.write("% \n")
   		outputfile.write("% Group: \n")
   		outputfile.write("%      Mikkel\n")
   		outputfile.write("%      Anna\n")
   		outputfile.write("%      Felix\n")
   		outputfile.write("% \n")
   		outputfile.write("@RELATION cancer\n")
   		outputfile.write("\n")
   		#outputfile.write("@ATTRIBUTE patientName STRING\n")
   		gene_id_list = gene_id.items()
   		gene_id_list_sorted = sorted(gene_id_list, key=lambda tup: tup[1])
   		for (gene_name, gene_ids) in gene_id_list_sorted:
   			temp_output = '@ATTRIBUTE ' + gene_name + ' INTEGER\n'
			outputfile.write(temp_output)
		outputfile.write("@ATTRIBUTE tissue {BOCA,BRCA,CLLE,CMDI,COAD,EOPC,ESAD,GBM,KIRC,LICA,LINC,LIRI,LUSC,MALY,NBL,ORCA,OV,PACA,PAEN,PBCA,PRAD,READ,RECA,SKCM,THCA,UCEC}\n")
		outputfile.write("\n")
		outputfile.write("@DATA\n")
		for element in training_data_set:
			outputfile.write(element)
			outputfile.write("\n")



	## CREATE TESTING FILE
	with open(testingFile, "a") as outputfile:
		outputfile.write("% DM838 - Project 2+4\n")
   		outputfile.write("% \n")
   		outputfile.write("% Group: \n")
   		outputfile.write("%      Mikkel\n")
   		outputfile.write("%      Anna\n")
   		outputfile.write("%      Felix\n")
   		outputfile.write("% \n")
   		outputfile.write("@RELATION cancer\n")
   		outputfile.write("\n")
   		#outputfile.write("@ATTRIBUTE patientName STRING\n")
   		gene_id_list = gene_id.items()
   		gene_id_list_sorted = sorted(gene_id_list, key=lambda tup: tup[1])
   		for (gene_name, gene_ids) in gene_id_list_sorted:
   			temp_output = '@ATTRIBUTE ' + gene_name + ' INTEGER\n'
			outputfile.write(temp_output)
		outputfile.write("@ATTRIBUTE tissue {BOCA,BRCA,CLLE,CMDI,COAD,EOPC,ESAD,GBM,KIRC,LICA,LINC,LIRI,LUSC,MALY,NBL,ORCA,OV,PACA,PAEN,PBCA,PRAD,READ,RECA,SKCM,THCA,UCEC}\n")
		outputfile.write("\n")
		outputfile.write("@DATA\n")
		for element in test_data_set:
			outputfile.write(element)
			outputfile.write("\n")
else:
	print "Couldn't find merged.csv in current directory."